from core.views.base_view import BaseView
from django.http import HttpResponse


class HomeView(BaseView):
    def get(self, request, *args, **kwargs):
        return HttpResponse("Hello! Zabia is here to help")