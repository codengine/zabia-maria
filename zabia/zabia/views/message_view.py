import json
import smooch
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponseRedirect, HttpResponse
from django.utils.decorators import method_decorator
from core.views.base_view import BaseView

from zabia.bot import Bot


class MessageView(BaseView):
    def get(self, request, *args, **kwargs):
        if request.GET['hub.verify_token'] == 'VERIFY_TOKEN':
            return HttpResponse(request.GET['hub.challenge'])
        else:
            return HttpResponse('Invalid verify token')

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            bot = Bot()
            api_instance = bot.get_api_instance()

            body = request.body.decode('utf-8')
            json_body = json.loads(body)
            message_replied = bot.handle_message(json_body)
            print(message_replied)
        except Exception as exp:
            print(str(exp))
        return HttpResponse({'a': 1}, content_type='application/json')