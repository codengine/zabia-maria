import os
import csv
import json
from bs4 import BeautifulSoup
from web_browser import WebBrowser


class Parser(object):

    def parse_all_categories_links(self, page):
        links = {}
        soup = BeautifulSoup(page)
        all_alphabet_items = soup.find_all('ul', {'class': 'f-dropdown'})
        for item in all_alphabet_items:
            anchors = item.find_all('a', href=True)
            alphabet = item.get("id").replace("drop", "")
            links[alphabet] = []
            for anchor in anchors:
                links[alphabet] += [anchor['href']]
        return links

    def find_all_text(self, element):
        i = 0
        max_count = 1000
        text = []
        next_siblings = element.find("font")
        while True:
            if not next_siblings:
                break
            text += [next_siblings.text]
            next_siblings = next_siblings.find_next_sibling()
            if i > max_count:
                break
        return text

    def parse_food_details(self, page):
        details_list = []
        soup = BeautifulSoup(page)
        content_container = soup.find('div', {'class': 'content-container'}).find_next('div', {'class': 'row'}).find_next('div', {'class': 'row'}).find_next('div', {'class': 'row'}).find_next('div', {'class': 'row'})
        food_content = content_container.contents[1]
        # Find heading 1
        heading_1 = content_container.find("h1").text
        print(heading_1)
        main_content = content_container.find("div", {"class": "subhead"}).find_next('div')
        h1_image = content_container.find("figure", {"class": "headline-img"}).find("img")
        h1_image_link = "https:" + h1_image["src"]

        # Find all h1 text
        max_count = 1000
        i = 0
        paragraph_text = []
        nextNode = content_container.find("figure", {"class": "headline-img"}).find_next_sibling()
        while True:
            try:
                tag_name = nextNode.name
            except AttributeError:
                tag_name = ""
            # print(nextNode.text)
            if tag_name != "p":
                break
            else:
                if i > max_count:
                    break
            # text = self.find_all_text(nextNode)
            # if not text:
            #     break
            nodeText = nextNode.text
            if nodeText:
                paragraph_text += [nextNode.text]
            if not nextNode:
                break
            nextNode = nextNode.find_next_sibling()
            i += 1

        details = {}
        details["title"] = heading_1
        details["image"] = h1_image_link
        if paragraph_text:
            details["content"] = " ".join(paragraph_text)
        details_list += [details]

        # Find all h2
        all_h2 = content_container.find_all("h2")
        for h2 in all_h2:

            parent_element = h2.parent
            try:
                classes = dict(parent_element.attrs)["class"]
            except:
                classes = []
            if "summary" in classes:
                print("Found summary")
                continue

            try:
                nutrition_facts = h2.parent.parent.parent.parent
                classes = dict(parent_element.attrs)["class"]
            except:
                classes = []

            if "nutrition-facts" in classes:
                print("Found nutrition facts")
                continue

            h2_details = {}
            title = h2.text
            dt = []
            j, count_max = 0, 1000
            next_node = h2.find_next_sibling()
            while True:
                try:
                    tag_name = next_node.name
                except AttributeError:
                    tag_name = ""
                # print(nextNode.text)
                if tag_name != "p":
                    break
                else:
                    if j > count_max:
                        break
                node_text = next_node.text
                if node_text:
                    dt += [next_node.text]
                if not next_node:
                    break
                next_node = next_node.find_next_sibling()
                j += 1

            h2_details["title"] = title
            h2_details["content"] = " ".join(dt)

            details_list += [h2_details]

        return details_list


class MercolaFoodsScraper(object):

    def __init__(self):
        self.browser = WebBrowser()
        self.parser = Parser()

    def scrape_all_categories(self):
        base_url = "https://alimentossaludables.mercola.com/tabla-de-contenido.html"
        self.browser.open(base_url)
        page = self.browser.get_page()
        all_categories = self.parser.parse_all_categories_links(page)
        print("Got all categories. Saving now...")
        with open("categories.json", "w") as f:
            contents = json.dumps(all_categories)
            f.write(contents)
        print("Categiries written to file: categiries.json")

    def scrape_category_detail(self, url):
        print("Entered: %s" % url)
        self.browser.open(url)
        page = self.browser.get_page()
        print("Fething Details for: %s" % url)
        details = self.parser.parse_food_details(page)
        if details:
            print("Details found: %s" % details)
            print("Reading existing food details")
            food_details = {}
            with open("food_detail.json", "r") as f:
                content = f.read()
                food_details = json.loads(content)
            if url not in food_details:
                food_details[url] = details
            print("Writing food details to the json file: food_detail.json")
            with open("food_detail.json", "w") as f:
                contents = json.dumps(food_details)
                f.write(contents)
            print("Details written in file food_detail.json")
        else:
            print("Details not found")

    def start_scraping(self):
        # categories = {}
        # with open("categories.json", "r") as f:
        #     categories = json.loads(f.read())
        #
        # all_links = {}
        # for alpha, items in categories.items():
        #     all_links[alpha] = {}
        #     for link in items:
        #         all_links[alpha][link] = False
        #
        # with open("food_detail.json", "w") as f:
        #     contents = json.dumps(all_links)
        #     f.write(contents)
        # print("Done")

        all_links = {}
        with open("food_detail.json", "r") as f:
            all_links = json.loads(f.read())

        headers_added = False

        for alpha, links in all_links.items():
            for link, status in links.items():
                print(link)
                if not status:
                    self.browser.open(link)
                    page = self.browser.get_page()
                    food_details = self.parser.parse_food_details(page)

                    # print(len(food_details))
                    # print(food_details)

                    if food_details:
                        if not headers_added:
                            csv_headers = ["URL"]
                            csv_headers += ["Title"]
                            for info in food_details:
                                csv_headers += [info["title"]]
                                if "image" in info:
                                    csv_headers += ["Image"]
                            with open(r'output.csv', 'w') as f:
                                writer = csv.writer(f)
                                writer.writerow(csv_headers)

                            headers_added = True

                        csv_contents = [link]
                        for i, info in enumerate(food_details):
                            if i == 0:
                                csv_contents += [info["title"]]
                            csv_contents += [info["content"]]
                            if "image" in info:
                                csv_contents += [info["image"]]

                        with open(r'output.csv', 'a') as f:
                            writer = csv.writer(f)
                            writer.writerow(csv_contents)

                    details_json = {}
                    with open("food_detail.json", "r") as f:
                        details_json = json.loads(f.read())
                    details_json[alpha][link] = True
                    with open("food_detail.json", "w") as f:
                        contents = json.dumps(details_json)
                        f.write(contents)

    def close_browser(self):
        self.browser.close()


if __name__ == "__main__":
    scraper = MercolaFoodsScraper()
    scraper.start_scraping()
    scraper.close_browser()
